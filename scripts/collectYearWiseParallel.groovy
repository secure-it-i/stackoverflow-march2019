/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovy.json.JsonSlurper
import static groovyx.gpars.GParsPool.createPool
import static groovyx.gpars.GParsPool.withExistingPool

final cli = new CliBuilder(usage: 'groovy collectYearWiseParallel.groovy')
cli.h(longOpt: 'APIHits', args: 1, argName: 'APIHits', required: true,
        'BENCHMARK_HITS_OUTPUT file')
cli.c(longOpt: 'CADates', args: 1, argName: 'CADates', required: true,
        'qid-creation-activity.txt')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final POOL_SIZE = 32
final pool = createPool(POOL_SIZE)
final jsonSlurper = new JsonSlurper()
final reader = new File(options.h).newReader()

final qid2year = new File(options.c).
        readLines().
        collect { [it.split(',')[0] as Long, it.split(',')[1] as int] }.
        collectEntries()

println "qid2year: ${qid2year.size()}"

reader.readLine()
def out = new File(options.o)
out.write 'API,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,Total'
withExistingPool(pool) {
    reader.eachParallel { line ->
        def year2score = [:]
        (2008..2019).each {
            year2score[it] = [] as Set
        }

        def api = jsonSlurper.parseText(line).api
        jsonSlurper.parseText(line).qid.each { id ->
            year2score[qid2year[id as Long]] << id
        }

        def result = "${api.replaceAll(',', '!')}"
        year2score.each { k, v ->
            year2score[k] = v.size()
            result += ",${year2score[k]}"
        }

        def sum = year2score.values().toList().collect { it as int }.sum()
        result += ",$sum"
        synchronized (out) {
            out.append "\n${result}"
        }

        println api
    }
}
