/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovyx.gpars.actor.DynamicDispatchActor

final cli = new CliBuilder(usage: 'groovy collectAnswerIdFromQuestionIds.groovy')
cli.p(longOpt: 'postsFile', args: 1, argName: 'postsFile', required: true,
        'StackOverflow Posts')
cli.c(longOpt: 'commentsOutputFile', args: 1, argName: 'commentsOutputFile', required: true,
        'comment-ids-related-to-android.txt')
cli.a(longOpt: 'qnaIds', args: 1, argName: 'qnaIds', required: true,
        'answer-ids-related-to-android.txt')
cli.v(longOpt: 'voteIds', args: 1, argName: 'voteIds', required: true,
        'vote-ids-related-to-android.txt')
cli.f(longOpt: 'commentsFile', args: 1, argName: 'commentsFile', required: true,
        'StackOverflow Comments')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final posts = new File(options.p)
final reader = posts.newReader()
final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 1e4 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers
final result = []
final YEAR = 2014

final c2qa = [:]
final qa2comments = [:] // Storing questions and answers as keys and their respective comments as value
final commentsFile = new File(options.c)
final commentReader = commentsFile.newReader()
commentReader.readLine()
commentReader.eachLine {
    def splittedLine = it.split(",")
    def qaId = splittedLine[0] as Long
    qa2comments[qaId] = []
    def count = splittedLine[1] as Long
    if (count > 0) {
        splittedLine[2].split(" ").each {
            def commentId = it as Long
            qa2comments[qaId] << commentId
            c2qa[commentId] = qaId
        }
    }
}

commentReader.close()
println "qa2comments size:${qa2comments.size()}"
println "c2qa size:${c2qa.size()}"

final a2q = [:]
final q2a = [:] // Storing questions and their respective answers
final qnaFile = new File(options.a)
final qnaReader = qnaFile.newReader()
qnaReader.readLine()
qnaReader.eachLine {
    def splittedLine = it.split(",")
    def qid = splittedLine[0] as Long
    q2a[qid] = []
    def count = splittedLine[1] as Long
    if (count > 0) {
        splittedLine[2].split(" ").each {
            def ansId = it as Long
            q2a[qid] << ansId
            a2q[ansId] = qid
        }
    }
}

qnaReader.close()
println "q2a size:${q2a.size()}"
println "a2q size:${a2q.size()}"

final qa2votes = [:] // Storing questions, answers, and comments as keys and their respective votes as value
final votesFile = new File(options.v)
final voteReader = votesFile.newReader()

voteReader.readLine()
voteReader.eachLine {
    def splittedLine = it.split(",")
    qa2votes[splittedLine[0] as Long] = []
    def count = splittedLine[1] as Long
    qa2votes[splittedLine[0] as Long] = count
}
voteReader.close()
println "qa2votes size:${qa2votes.size()}"

final Set filtered = []
def questions = []
final collector = new DynamicDispatchActor().become {
    when { List ids ->
        ids.each { filtered << it[0] }
    }
    when { boolean i -> terminate() }
}.start()

def readingComments = false
final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            if (!readingComments) {
                questions = lines.findAll {
                    ((xmlParse.parseText(it).@PostTypeId.text() as Long) == 1) && q2a.containsKey(xmlParse.parseText(it).@Id.text() as Long)
                }.collect {
                    [xmlParse.parseText(it).@Id.text() as Long, xmlParse.parseText(it).@LastActivityDate.text().split("-")[0] as int]
                }

                def res = questions.findAll { question ->
                    (question[1] > YEAR) || // LastActivityDate greater than 2014
                            (qa2votes.containsKey(question[0]) && (qa2votes.get(question[0]) > 0)) || // Has at least 1 upvote/downvote post 2014
                            q2a.get(question[0]).any { ansId -> // If any ansId has at least 1 upvote/downvote post 2014
                                (qa2votes.containsKey(ansId) && qa2votes.get(ansId) > 0)
                            }
                }
                collector.send res
            } else { // Checking comment's creation date.
                def res = lines.findAll {
                    c2qa.containsKey(xmlParse.parseText(it).@Id.text() as Long)
                }.collect {
                    [xmlParse.parseText(it).@Id.text() as Long, xmlParse.parseText(it).@PostId.text() as Long, xmlParse.parseText(it).@CreationDate.text().split("-")[0] as int]
                }.findAll {
                    it[2] > YEAR
                }.collect {
                    [a2q.containsKey(it[1]) ? a2q[it[1]] : it[1], it[2]] // Replacing PostId with its Qid
                }
                collector.send res
            }
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}


println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0

reader.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</posts>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed")
    }
}

reader.close()
processors[procNum % NUM_PROCS].send lines // Remaining lines

// Filtering based on Comments' CreationDate

final commentsXml = new File(options.f).newReader()
commentsXml.readLine()

lines = []
prevSyncLineNum = 0
readingComments = true
commentsXml.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</comments>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed")
    }
}
commentsXml.close()

processors[procNum % NUM_PROCS].send lines // Remaining lines
processors.each { it << false } // Terminating
processors*.join() // Waiting


collector << false // Terminating
collector.join() // Waiting

new File(options.o).withWriter { out ->
    println "Size " + filtered.size()
    filtered.each { id ->
        out.println id
    }
}
