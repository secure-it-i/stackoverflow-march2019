/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovy.transform.ToString
import groovyx.gpars.actor.DynamicDispatchActor
import groovy.transform.ToString

@ToString
final class LongPair {
    Long qid, aid
}

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 1e4 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers

final cli = new CliBuilder(usage: 'groovy collectCommentIdsFromQnAIds.groovy')
cli.c(longOpt: 'commentsFile', args: 1, argName: 'commentsFile', required: true,
        'StackOverflow Comments')
cli.a(longOpt: 'qnaIds', args: 1, argName: 'qnaIds', required: true,
        'answer-ids-related-to-android.txt')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final commentsFile = new File(options.c)
final reader = commentsFile.newReader()

final qnaFile = new File(options.a).readLines()
final qna2comments = [:] // Map to store the final result

//qnaFile.readLine()
qnaFile[1..qnaFile.size()-1].each {
    qna2comments[it.split(",")[0] as Long] = []
    if ((it.split(",")[1] as Long) > 0) {
        it.split(",")[2].split(" ").each {
            qna2comments[it as Long] = []
        }
    }
}
println qna2comments.size()

final collector = new DynamicDispatchActor().become {
    when { List pairs -> pairs.each { qna2comments[it.qid] << it.aid }
    }
    when { boolean i -> terminate() }
}.start()

final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            def res = lines.collect {
                [xmlParse.parseText(it).@PostId.text(), xmlParse.parseText(it).@Id.text()]
            }.findAll {
                qna2comments.containsKey(it[0] as Long)
            }.collect {
                new LongPair(
                        qid: it[0] as Long,
                        aid: it[1] as Long
                )
            }
            collector.send res
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</comments>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed \
${qna2comments.size()}")
    }
}

reader.close()

processors[procNum % NUM_PROCS].send lines // Remaining lines
processors.each { it << false } // Terminating
processors*.join() // Waiting

collector << false // Terminating
collector.join() // Waiting

new File(options.o).withWriter { out ->
    out.println "QnAId,# of Comments,CommentIds"
    qna2comments.each { k, v ->
        out.println("$k,${v.size()},${v.join(' ')}")
    }
}
