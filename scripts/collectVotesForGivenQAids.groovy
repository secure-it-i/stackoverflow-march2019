/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovyx.gpars.actor.DynamicDispatchActor
import groovy.transform.ToString

@ToString
final class LongPair {
    Long qid, aid
}

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 1e4 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers

final cli = new CliBuilder(usage: 'groovy collectAndroidSecurityQuestions.groovy')
cli.c(longOpt: 'commentsOutputFile', args: 1, argName: 'commentsOutputFile', required: true,
        'comment-ids-related-to-android.txt')
cli.v(longOpt: 'votes', args: 1, argName: 'votes', required: true,
        'StackOverflow/Votes.xml')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final commentsFile = new File(options.c)
final votes = new File(options.v)
final commentsFileReader = commentsFile.newReader()
final reader = votes.newReader()

final qa = [:]

commentsFileReader.readLine()

commentsFileReader.eachLine {
    qa[it.split(",")[0] as Long] = []
}

println qa.size()

final collector = new DynamicDispatchActor().become {
    when { List pairs -> pairs.each { qa[it.qid] << it.aid }
    }
    when { boolean i -> terminate() }
}.start()

final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            def res = lines.collect {
                [xmlParse.parseText(it).@PostId.text() as Long, xmlParse.parseText(it).@Id.text() as long, xmlParse.parseText(it).@CreationDate.text().split("-")[0] as Long]
            }.findAll {
                it[2] > 2014 && qa.containsKey(it[0] as Long)
            }.collect {
                new LongPair(
                        qid: it[0] as Long,
                        aid: it[1] as Long
                )
            }
            collector.send res
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</votes>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed")
    }
}

reader.close()

    processors[procNum % NUM_PROCS].send lines // Remaining lines
    processors.each { it << false } // Terminating
    processors*.join() // Waiting

    collector << false // Terminating
    collector.join() // Waiting

    new File(options.o).withWriter { out ->
        out.println "qa,# of Votes After 2014,VoteIds"
        qa.each { k, v ->
            out.println("$k,${v.size()},${v.join(' ')}")
        }
    }
