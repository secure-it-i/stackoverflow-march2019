/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovyx.gpars.actor.DynamicDispatchActor

final cli = new CliBuilder(usage: 'groovy collectAnswerIdFromQuestionIds.groovy')
cli.p(longOpt: 'postsFile', args: 1, argName: 'postsFile', required: true,
        'StackOverflow Posts')
cli.a(longOpt: 'qnaFile', args: 1, argName: 'qnaFile', required: true,
        'common/answer-ids-related-to-android.txt')
cli.f(longOpt: 'filteredQIDs', args: 1, argName: 'filteredQIDs', required: true,
        'Time filteredQIDs')
cli.q(longOpt: 'filteredQsOutputPath', args: 1, argName: 'filteredQsOutputPath', required: true,
        'Questions output path')
cli.z(longOpt: 'filteredAsOutputPath', args: 1, argName: 'filteredAsOutputPath', required: true,
        'Answers output path')

final options = cli.parse(args)

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 1e4 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers
final postsFile = new File(options.p)
final reader = postsFile.newReader()
final questionAnswer = new File(options.a).readLines()
final Qs = questionAnswer[1..questionAnswer.size() - 1].collect { it.split(',')[0] as Long }.sort()
final As = questionAnswer[1..questionAnswer.size() - 1].findAll { (it.split(',')[1] as Long) > 0 }.collect {
    it.split(',')[2].split(' ').collect { it as Long }
}.flatten().sort()
final List timeFiltered = new File(options.f).readLines().collect {it as Long}.sort()
final Set filteredQs = []
final Set filteredAs = []

println "# of Questions: ${Qs.size()}"
println "# of Answers: ${As.size()}"
println "# of Time Filtered QIDs: ${timeFiltered.size()}"

final collector = new DynamicDispatchActor().become {
    when { List ids ->
        ids.each { id ->
            if (id[1] == 1) {
                filteredQs << id[0]
                if(!id[2].equals("")) {
                    filteredAs << [id[2] as Long, id[0] as Long]
                }
            }
            else
                filteredAs << [id[0] as Long, id[3] as Long]
        }
    }
    when { boolean i -> terminate() }
}.start()

final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            def res = lines.findAll {
                def id = xmlParse.parseText(it).@Id.text() as Long
                (Collections.binarySearch(Qs, id) >= 0) || (Collections.binarySearch(As, id) >= 0)
            }.collect {
                def parse = xmlParse.parseText(it)
                [parse.@Id.text() as Long, parse.@PostTypeId.text() as int, parse.@Score.text() as int, parse.@CommentCount.text() as int, parse.@FavoriteCount.text(), parse.@AcceptedAnswerId.text(), parse.@ParentId.text()]
            }.findAll { post ->
                ((post[1] == 1) &&
                        (Collections.binarySearch(timeFiltered, post[0]) >= 0) &&
                        (post[2] != 0 || post[3] != 0 || (!post[4].equals("") && (post[4] as int) != 0))) \
                || ((post[1] == 2) &&
                        (Collections.binarySearch(timeFiltered, post[6] as Long) >= 0) &&
                        (post[2] != 0 || post[3] != 0))
                // If Question is having Score, CommentCount or FavCount != 0
                // If Answer is having Score, CommentCount != 0 or has been accepted.
            }.collect { post ->
                [post[0], post[1], post[5], post[6]]
            }
//            println res
            collector.send res
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</posts>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed ${filteredQs.size()} ${filteredAs.size()}")
    }
}

reader.close()

processors[procNum % NUM_PROCS].send lines // Remaining lines
processors.each { it << false } // Terminating
processors*.join() // Waiting

collector << false // Terminating
collector.join() // Waiting

new File(options.q).withWriter {out ->
    filteredQs.each {
        out.println it
    }
}

new File(options.z).withWriter {out ->
    filteredAs.each {
        out.println "${it[0]},${it[1]}"
    }
}
