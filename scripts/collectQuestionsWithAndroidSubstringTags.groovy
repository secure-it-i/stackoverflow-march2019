/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovyx.gpars.actor.DynamicDispatchActor

final GIVEN_TAG = 'android'
final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 1e4 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers

final cli = new CliBuilder(usage: 'groovy collectQuestionsWithAndroidSubstringTags.groovy')
cli.p(longOpt: 'postsFile', args: 1, argName: 'postsFile', required: true,
        'StackOverflow Posts')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final postsFile = new File(options.p)
final reader = postsFile.newReader()

final tag2ids = [:] // Map to store the final result

final collector = new DynamicDispatchActor().become {
    when { Map id2tags ->
        id2tags.each { id, tags ->
            tags.each {
                if(tag2ids.containsKey(it))
                    tag2ids[it] << id
                else
                    tag2ids[it] = [id]
            }
        }
    }
    when { boolean i -> terminate() }
}.start()

final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            def res = lines.findAll {
                xmlParse.parseText(it).@Tags.text().contains("android")
            }.collect {
                [xmlParse.parseText(it).@Id.text() as Long, xmlParse.parseText(it).@Tags.text().replace("<", "").replace(">", ",").split(",")]
            }
            .collectEntries()
            collector.send res
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</posts>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed \
${tag2ids.size()}")
    }
}

reader.close()

processors[procNum % NUM_PROCS].send lines // Remaining lines
processors.each { it << false } // Terminating
processors*.join() // Waiting

collector << false // Terminating
collector.join() // Waiting

new File(options.o).withWriter { out ->
    out.println "Tag,# of Question Posts,PostIds"
    tag2ids.each { k, v ->
        out.println("$k,${v.size()},${v.join(' ')}")
    }
}
