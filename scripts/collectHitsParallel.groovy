/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

import java.util.regex.Matcher
import java.util.regex.Pattern

import static groovyx.gpars.GParsPool.createPool
import static groovyx.gpars.GParsPool.withExistingPool

final cli = new CliBuilder(usage: 'groovy collectHitsParallel.groovy')
cli.b(longOpt: 'QTreeBody', args: 1, argName: 'QTreeBody', required: true,
        'question-trees-with-body-json.txt')
cli.a(longOpt: 'APIs', args: 1, argName: 'APIs', required: true,
        'API list')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)
innerPool = createPool(32)

bodyFile = new File(options.b).readLines().findAll { !it.startsWith('{"type":"c"') }
final realWorldApis = new File(options.a).readLines()

def getMethodName(String api) {
    if (api.startsWith('E,')) return api.split(",")[1]
    if (api.startsWith('A')) {
        if (api.contains('android:name')) return api.split(",")[2].split("=")[1]
        else return api.split(",")[2]
    }
    if (api.split(",")[2].equals('<init>')) {
        return getClassName(api)
    }
    return api.split(",")[2]
}


def getClassName(String api) {
    if (api.startsWith('E')) return null
    return api.split(',')[1].split('/').last().replaceAll('\\$', '.')
}


def findAnApiOccurence(String api, String methodName, String className, final String body) {
    int result = 0
    final Pattern patternMethod = api.startsWith('M') ? Pattern.compile("(?<![a-zA-z])" + methodName + "(?![a-zA-z])") :
            (api.startsWith('F') ? Pattern.compile("[a-zA-z0-9]\\." + methodName + "(?![a-zA-z])") :
                    Pattern.compile("(?<![a-zA-z])" + methodName + "(?![a-zA-z])"))

    Matcher matcher = patternMethod.matcher(body)
    if (api.startsWith('E,')) { // Events
        if (matcher.find()) {
            result = 1
        }
    } else { // Method, Fields, and Attributes
        if (matcher.find() && checkClass(className, body))
            result = 1
    }

    return result
}

def checkClass(className, code) {
    final patternClass = Pattern.compile("(?<![a-zA-z])" + className + "(?![a-zA-z])")
    def matcher = patternClass.matcher(code)
    return matcher.find()
}

def checkPackage(packageName, code) {
    final patternPackage = Pattern.compile("(?<![a-zA-z])" + packageName + "(?![a-zA-z])")
    def matcher = patternPackage.matcher(code)
    return matcher.find()
}

withExistingPool(innerPool) {
    File out = new File(options.o)
    out.write "API,QIDs"
    int count = 0
    JsonBuilder jsonBuilder = new groovy.json.JsonBuilder()
    JsonSlurper jsonSlurper = new JsonSlurper()
    realWorldApis.each { apiName ->
        Map apiInfo = [:]
        apiInfo[apiName] = [] as Set
        String methodName = getMethodName(apiName)
        String className = getClassName(apiName)
        bodyFile.eachParallel { line ->
            def object = jsonSlurper.parseText(line)
            String parentId = object.parentId
            String body = object.text

            int result = findAnApiOccurence(apiName, methodName, className, body)
            if (result == 1) {
                synchronized (apiInfo) {
                    apiInfo[apiName] << Long.parseLong(parentId)
                }
            }
        }

        jsonBuilder {
            api apiName
            qid apiInfo[apiName]
        }

        out.append "\n${jsonBuilder.toString()}"
        println "${apiName}\t${++count}"
    }
}
