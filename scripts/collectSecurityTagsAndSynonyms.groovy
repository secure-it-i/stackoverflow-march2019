/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

final cli = new CliBuilder(usage: 'groovy collectSecurityTagsAndSynonyms.groovy')
cli.t(longOpt: 'securityTags', args: 1, argName: 'securityTags', required: true,
        'Security.StackExchange Tags.xml')
cli.s(longOpt: 'securityTagSynFile', args: 1, argName: 'securityTagSynFile', required: true,
        'synonyms.csv downloaded from SEDE')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

def securityTags = new File(options.t).readLines()
def securityTagSynFile = new File(options.s).readLines()

def transitiveClosure(List list, tagSyn, revTagSyn) {
    def flag = true
    def tags = [list[0]] as Queue
    def size = list[1].size()
    while (flag) {
        tag = tags.remove()
        if (tagSyn.containsKey(tag) && tagSyn[tag] != list[0]) {
            list[1].addAll(tagSyn[tag])
            tags.add(tagSyn[tag])
        }

        if (revTagSyn.containsKey(tag) && revTagSyn != list[0]) {
            list[1].addAll(revTagSyn[tag])
            tags.add(revTagSyn[tag])
        }

        if (size != list[1].size())
            size = list[1].size()
        else
            flag = false
    }
}


def securityTagSyn = [:]
securityTagSynFile[1..securityTagSynFile.size()-1].collect {
    [it.split(",")[1].replaceAll("\"", ""), it.split(",")[2].replaceAll("\"", "")]
}.collectEntries {
    if (securityTagSyn.containsKey(it[0])) securityTagSyn[it[0]] << it[1]
    else securityTagSyn[it[0]] = [it[1]]
}

def securityRevTagSyn = [:]
securityTagSynFile[1..securityTagSynFile.size()-1].collect {
    [it.split(",")[2].replaceAll("\"", ""), it.split(",")[1].replaceAll("\"", "")]
}.collectEntries {
    if (securityRevTagSyn.containsKey(it[0])) securityRevTagSyn[it[0]] << it[1]
    else securityRevTagSyn[it[0]] = [it[1]]
}

def xmlParse = new XmlSlurper()
securityTags = securityTags[2..securityTags.size() - 2].collect { [xmlParse.parseText(it).@TagName.text(), [] as Set] }
securityTags << ["security",[]]
securityTags.each {
    transitiveClosure(it, securityTagSyn, securityRevTagSyn)
}

new File(options.o).withWriter {out ->
    securityTags.flatten().unique().sort().each {
        out.println it
    }
}
