/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovyx.gpars.actor.DynamicDispatchActor
import groovy.transform.ToString

@ToString
final class LongPair {
    Long qid, aid
}

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 1e4 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers

final cli = new CliBuilder(usage: 'groovy collectAnswerIdFromQuestionIds.groovy')
cli.p(longOpt: 'postsFile', args: 1, argName: 'postsFile', required: true,
        'StackOverflow Posts')
cli.q(longOpt: 'qIds', args: 1, argName: 'qIds', required: true,
        'question-ids-with-android.txt')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final postsFile = new File(options.p)
final reader = postsFile.newReader()

final questionFile = new File(options.q).newReader()
final post2answers = [:] // Map to store the final result
questionFile.readLine()

questionFile.eachLine {
    if (!it.split(",")[1].equals("0")) {
        it.split(",")[2].split(" ").each { id ->
            post2answers[id as Long] = []
        }
    }
}

final collector = new DynamicDispatchActor().become {
    when { List pairs -> pairs.each { post2answers[it.qid] << it.aid }
    }
    when { boolean i -> terminate() }
}.start()

final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            def res = lines.findAll {
                (xmlParse.parseText(it).@PostTypeId.text() as int) == 2
            }.collect {
                [xmlParse.parseText(it).@ParentId.text(), xmlParse.parseText(it).@Id.text()]
            }.findAll {
                post2answers.containsKey(it[0] as Long)
            }.collect {
                new LongPair(
                        qid: it[0] as Long,
                        aid: it[1] as Long
                )
            }
            collector.send res
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</posts>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed \
${post2answers.size()}")
    }
}

reader.close()

processors[procNum % NUM_PROCS].send lines // Remaining lines
processors.each { it << false } // Terminating
processors*.join() // Waiting

collector << false // Terminating
collector.join() // Waiting

new File(options.o).withWriter { out ->
    out.println "QuestionId,# of Answers,AnswerIds"
    post2answers.each { k, v ->
        out.println("$k,${v.size()},${v.join(' ')}")
    }
}
