/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovy.json.JsonSlurper
import static groovyx.gpars.GParsPool.createPool
import static groovyx.gpars.GParsPool.withExistingPool

final cli = new CliBuilder(usage: 'groovy collectYearWiseSecurityParallel.groovy')
cli.h(longOpt: 'APIHits', args: 1, argName: 'APIHits', required: true,
        'BENCHMARK_HITS_OUTPUT file')
cli.c(longOpt: 'CADates', args: 1, argName: 'CADates', required: true,
        'qid-creation-activity.txt')
cli.s(longOpt:'Security QIDs', args: 1, argName: 'secQIDs', required: true,
        'Android Security QIDs')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final POOL_SIZE = 32
final pool = createPool(POOL_SIZE)
final jsonSlurper = new JsonSlurper()
final reader = new File(options.h).newReader()
final secFile = new File(options.s).readLines()
List security = ((Set)secFile[1..secFile.size()-1].
        findAll {(it.split(',')[1] as int) > 0}.
        collect {it.split(',')[2].split(' ').collect {it as Long}}.
        flatten()).
        toList().sort()

final qid2year = new File(options.c).
        readLines().
        collect { [it.split(',')[0] as Long, it.split(',')[1] as int] }.
        collectEntries()

println "# of security QIDs: ${security.size()}"

reader.readLine()
def out = new File(options.o)
out.write 'API,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,Total'
withExistingPool(pool) {
    reader.eachParallel { line ->
        def secScore = [:]
        (2008..2019).each {
            secScore[it] = [] as Set
        }

        def api = jsonSlurper.parseText(line).api
        List qids = jsonSlurper.parseText(line).qid
        qids.findAll {id -> Collections.binarySearch(security, id as Long) >= 0}.each { id ->
            secScore[qid2year[id as Long]] << id
        }
        def result = "${api.replaceAll(',', '!')}"
        secScore.each { k, v ->
            secScore[k] = v.size()
            result += ",${secScore[k]}"
        }
        def sum = secScore.values().toList().collect { it as int }.sum()
        result += ",$sum"
        synchronized (out) {
            out.append "\n${result}"
        }
        println api
    }
}
