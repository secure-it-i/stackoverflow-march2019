/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovyx.gpars.actor.DynamicDispatchActor

final cli = new CliBuilder(usage: 'groovy collectAnswerIdFromQuestionIds.groovy')
cli.p(longOpt: 'postsFile', args: 1, argName: 'postsFile', required: true,
        'StackOverflow Posts')
cli.q(longOpt: 'filteredQsOutputPath', args: 1, argName: 'filteredQsOutputPath', required: true,
        'Questions output path')
cli.z(longOpt: 'filteredAsOutputPath', args: 1, argName: 'filteredAsOutputPath', required: true,
        'Answers output path')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 10000 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers
final postsFile = new File(options.p)
final reader = postsFile.newReader()

final ans2q = new File(options.z).readLines().collect { [it.split(',')[0] as Long, it.split(',')[1] as Long] }.collectEntries()
filteredQs = new File(options.q).readLines().collect { it as Long }.sort()

println "filteredAs: ${ans2q.size()}"
println "filteredQs: ${filteredQs.size()}"

id2body = [:] // Map to store the final result
filteredQs.each {
    id2body[it] = []
}

final collector = new DynamicDispatchActor().become {
    when { List list ->
        list.each { k, v ->
            if(!id2body.containsKey(k)) {
                id2body[k] = []
            }
            if (v.size() == 1)
                id2body[k].add(0, v)
            else {
                id2body[k] << v
            }
        }
    }
    when { boolean i -> terminate() }
}.start()


final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            def res = lines.findAll {
                def someId = xmlParse.parseText(it).@Id.text() as Long
                (Collections.binarySearch(filteredQs, someId) >= 0) || ans2q.containsKey(someId)
            }.collect {
                def someId = xmlParse.parseText(it).@Id.text() as Long
                if (xmlParse.parseText(it).@PostTypeId.text() as int == 1)
                    [someId, [xmlParse.parseText(it).@Body.text().replaceAll("\\r\\n|\\r|\\n", " ")]]
                else if (xmlParse.parseText(it).@PostTypeId.text() as int == 2)
                    [ans2q[someId], ['a', someId, ans2q[someId], xmlParse.parseText(it).@Body.text().replaceAll("\\r\\n|\\r|\\n", " ")]]
            }
            collector.send res

        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0

reader.eachLine { line, lineNum ->
  synchronized(lines) {
    if (lineNum > 2 && !line.equals("</posts>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed ${id2body.size()}")
    }
  }
}

reader.close()


processors[procNum % NUM_PROCS].send lines // Remaining lines
processors.each { it << false } // Terminating
processors*.join() // Waiting

collector << false // Terminating
collector.join() // Waiting

new File(options.o).withWriter { out ->
    def jsonBuilder = new groovy.json.JsonBuilder()
    id2body.each { k, v ->
        v.each { List body ->
            if (body.size() == 1) {
                jsonBuilder {
                    type 'q'
                    id k
                    parentId k
                    text body[0]
                }
                out.println jsonBuilder.toString()
            } else {
                jsonBuilder {
                    type body[0]
                    id body[1]
                    parentId body[2]
                    text body[3]
                }
                out.println jsonBuilder.toString()
            }
        }
    }
}
