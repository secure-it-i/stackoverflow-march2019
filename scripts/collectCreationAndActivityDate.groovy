/*
 * Copyright (c) 2019, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Aditya Narkar
 */

import groovyx.gpars.actor.DynamicDispatchActor

final cli = new CliBuilder(usage: 'groovy collectAnswerIdFromQuestionIds.groovy')
cli.p(longOpt: 'postsFile', args: 1, argName: 'postsFile', required: true,
        'StackOverflow Posts')
cli.f(longOpt: 'filteredQIDs', args: 1, argName: 'filteredQIDs', required: true,
        'common/filtered-question-ids-related-to-android.txt')
cli.o(longOpt: 'outputFile', args: 1, argName: 'outputFile', required: true,
        'Path of Output File')

final options = cli.parse(args)

final NUM_PROCS = Runtime.getRuntime().availableProcessors() - 2
final BATCH_SIZE = 1e4 // number of lines in a batch
final SYNC_LIMIT = 1e6 + 3  // number of lines to sync between master and workers
final postsFile = new File(options.p)
final reader = postsFile.newReader()
final qid2year = new File(options.f).
        readLines().
        collect {
            [it as Long,[]]
        }.collectEntries()

final collector = new DynamicDispatchActor().become {
    when { Map id2ca ->
        id2ca.each { id, years ->
            qid2year[id] = years
        }
    }
    when { boolean i -> terminate() }
}.start()

final processors = (0..<NUM_PROCS).collect { index ->
    new DynamicDispatchActor().become {
        when { List lines ->
            def xmlParse = new XmlSlurper()
            def res = lines.findAll {
                qid2year.containsKey(xmlParse.parseText(it).@Id.text() as Long)
            }.collect {
                [xmlParse.parseText(it).@Id.text() as Long,
                 [xmlParse.parseText(it).@CreationDate.text().split('-')[0],
                 xmlParse.parseText(it).@LastActivityDate.text().split('-')[0]]]
            }.collectEntries()

//            println res
            collector.send res
        }
        when { String s -> replyIfExists 0 }
        when { boolean i -> terminate() }
    }.start()
}

println("${new Date()} 0 lines processed")
def procNum = 0
def lines = []
def prevSyncLineNum = 0
reader.eachLine { line, lineNum ->
    if (lineNum > 2 && !line.equals("</posts>")) {
        lines << line
    }
    if (lines.size() > BATCH_SIZE) {
        processors[procNum % NUM_PROCS].send lines // Sending a list to the Actor
        if (lineNum - prevSyncLineNum > SYNC_LIMIT) {
            processors[procNum % NUM_PROCS].sendAndWait 'sync' // Waiting
            prevSyncLineNum = lineNum
        }
        procNum++
        lines = []
    }
    if (lineNum % 500000 == 0) {
        println("${new Date()} $lineNum lines processed \
${qid2year.size()}")
    }
}

reader.close()

processors[procNum % NUM_PROCS].send lines // Remaining lines
processors.each { it << false } // Terminating
processors*.join() // Waiting

collector << false // Terminating
collector.join() // Waiting

new File(options.o).withWriter {out ->
    qid2year.each {id, years ->
        out.println "$id,${years[0]},${years[1]}"
    }
}
