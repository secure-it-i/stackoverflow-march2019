### Summary

This repository contains scripts to process *Stack Overflow data dump* available as part of [Stack Exchange Data Dumps](https://archive.org/download/stackexchange) to calculate API hits for each API in the Droidbench, Ghera, Iccbench, and UBCbench.

### Execution

* Create folder named `StackOverflow` in data folder (`DATA_DIR`).

* Download Posts, Comments, and Votes related to *StackOverflow* from *StackOverflow data dump* and unzip them in `data/StackOverflow`. For our experiment, we collected these files which were dated Mar 04, 2019.

* Assign the absolute path for project to **TOP_LEVEL** variable in *master.sh*.

* Run:

 * On Unix: ./master.sh

 * On Windows: Open `TOP_LEVEL` in Cygwin.

    * dos2unix.exe master.sh  

    * ./master.sh  

### Definitions to understand the result:

* **time-filter:** This filter identifies the activity pertaining to a question. A question satisfies `time-filter` if it is created, answered, edited, commented on, upvoted/downvoted; or has an answer which is created, edited, commented on, or upvoted/downvoted after 2015.

* **filter-1:** This filter is identifies the activity pertaining to a question which satisfies the `time-filter` or an answer whose parentId (Question Id related to the answer) satisfies the `time-filter`. A question satisfies a `filter-1` if it has a non-zero score (upvotes-downvotes), or a non-zero comment count, or a non-zero favorite count. An answer satisfies a `filter-1` if it has a non-zero score (upvotes-downvotes), or a non-zero comment count, or it has been accepted.

* **Hit:** A hit is counted for an API in a question or an answer body is in following ways:

     - **Method**: If a method name and a class name is present in a body.

     - **Field**: If a field name and a class name is present in a body.

     - **Attribute**: If an attribute name and an element name is present in a body.

     - **Event**: If event name is present in a body.    

### Results:

The execution will generate the following files in `DATA_DIR` folder:

* **common/question-ids-with-android.txt:** Question post ids associated with *android* substring tags.

* **common/answer-ids-related-to-android.txt:** Answer post ids associated with *android* substring question ids.

* **common/comment-ids-related-to-android.txt:** Comment post ids associated with *android* substring question and answer ids.

* **common/vote-ids-related-to-android.txt:** Vote ids associated with *android* substring question and answer ids.

* **common/filtered-question-ids-related-to-android.txt:** Question post ids associated with *android* substring question ids that satisfies `time-filter`.

* **common/security-tags-and-synonyms.txt:** All the tags from Security.StackExchange with their synonyms.

* **common/security-question-ids-with-android.txt:** Question post ids having at least one *android* substring tag and at least one tag from *common/security-tags-and-synonyms.txt*.

* **common/filter-1-Questions.txt:** Question post ids associated with *android* substring question ids that satisfies `time-filter` and `filter-1`.

* **common/filter-1-ANSWERS.txt:** Answer post ids associated with *android* substring question ids that satisfies `time-filter` and `filter-1`.

* **common/question-trees-with-body-json.txt:** Body text for all the questions and answers that satisfies `filter-1`.

* **common/qid-creation-activity.txt:** Creation and activity dates for all the question ids that satisfies `time-filter`.

The execution will generate the following files in each benchmark suite's folder. Each benchmark suite's folder in `DATA_DIR` folder:

* **Benchmark/Results/question-tree-hits-json.txt:** Question post ids for each API in a benchmark suite. These question post ids contain a hit for an API in either of their question or one of their answer bodies. Note the question or answer that got hit also satisfies `filter-1`.

* **Benchmark/Results/.*-relevant.csv:** Year wise hit count and total for each API in a benchmark.

* **Benchmark/Results/.*-security.csv:** Year wise hit count and total for each API in a benchmark for question post ids which marked as *android-security* related by *common/security-tags-and-synonyms.txt*.


### Attribution

Copyright (c) 2019/2018, Kansas State University

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

Contributors:

1. Aditya Narkar (Creator, Developer)
2. Joydeep Mitra (Co-creator)
3. Venkatesh-Prasad Ranganath (Advisor)
