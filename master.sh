 # Copyright (c) 2019, Kansas State University
 #
 # Licensed under BSD 3-clause License
 #
 # Author: Aditya Narkar

# Initialization
TOP_LEVEL=Path/to/stackoverflow-march2019

# Directory path
DATA_DIR=$TOP_LEVEL/data
SCRIPTS_DIR=$TOP_LEVEL/scripts

# Data
POSTS_FILE=$DATA_DIR/StackOverflow/Posts.xml
COMMENTS_FILE=$DATA_DIR/StackOverflow/Comments.xml
VOTES_FILE=$DATA_DIR/StackOverflow/Votes.xml
SECURITY_TAGS=$DATA_DIR/SStEx/Tags.xml
SECURITY_SYNONYMS=$DATA_DIR/SStEx/synonyms.csv

# APIs
GHERA_APIS=$DATA_DIR/Ghera/APIs/ghera-benign-filtered-id-apis.txt
DROID_APIS=$DATA_DIR/Droidbench/APIs/droidbench-extended-filtered-id-apis.txt
ICC_APIS=$DATA_DIR/Iccbench/APIs/iccbench-filtered-id-apis.txt
UBC_APIS=$DATA_DIR/Ubcbench/APIs/ubcbench-filtered-id-apis.txt
REAL_WORLD_APIS=$DATA_DIR/Realworld/APIs/real-world-apis.txt

# Common Output
QUESTION_OUTPUT=$DATA_DIR/common/question-ids-with-android.txt
ANSWER_OUTPUT=$DATA_DIR/common/answer-ids-related-to-android.txt
COMMENTS_OUTPUT=$DATA_DIR/common/comment-ids-related-to-android.txt
SECURITY_OUTPUT=$DATA_DIR/common/security-question-ids-with-android.txt
VOTES_OUTPUT=$DATA_DIR/common/vote-ids-related-to-android.txt
FILTER_OUTPUT=$DATA_DIR/common/filtered-question-ids-related-to-android.txt
SECURITY_TAGS_AND_SYNONYMS_OUTPUT=$DATA_DIR/common/security-tags-and-synonyms.txt
BODY_OUTPUT=$DATA_DIR/common/question-trees-with-body-json.txt
CREATION_AND_ACTIVITY_OUTPUT=$DATA_DIR/common/qid-creation-activity.txt
FILTER_1_OUTPUT_QUESTIONS=$DATA_DIR/common/filter-1-Questions.txt
FILTER_1_OUTPUT_ANSWERS=$DATA_DIR/common/filter-1-ANSWERS.txt
SECURITY_TAGS_AND_SYNONYMS=$DATA_DIR/common/security-tags-and-synonyms.txt


# Benchmark specific output
GHERA_HITS_OUTPUT=$DATA_DIR/Ghera/Results/question-tree-hits-json.txt
GHERA_RELEVANT_OUTPUT=$DATA_DIR/Ghera/Results/ghera-relevant.csv
GHERA_SECURITY_OUTPUT=$DATA_DIR/Ghera/Results/ghera-security.csv

DROIDBENCH_HITS_OUTPUT=$DATA_DIR/Droidbench/Results/question-tree-hits-json.txt
DROIDBENCH_RELEVANT_OUTPUT=$DATA_DIR/Droidbench/Results/droid-relevant.csv
DROIDBENCH_SECURITY_OUTPUT=$DATA_DIR/Droidbench/Results/droid-security.csv

ICCBENCH_HITS_OUTPUT=$DATA_DIR/Iccbench/Results/question-tree-hits-json.txt
ICCBENCH_RELEVANT_OUTPUT=$DATA_DIR/Iccbench/Results/icc-relevant.csv
ICCBENCH_SECURITY_OUTPUT=$DATA_DIR/Iccbench/Results/icc-security.csv

UBCBENCH_HITS_OUTPUT=$DATA_DIR/Ubcbench/Results/question-tree-hits-json.txt
UBCBENCH_RELEVANT_OUTPUT=$DATA_DIR/Ubcbench/Results/ubc-relevant.csv
UBCBENCH_SECURITY_OUTPUT=$DATA_DIR/Ubcbench/Results/ubc-security.csv

REALWORLD_HITS_OUTPUT=$DATA_DIR/Realworld/Results/question-tree-hits-json.txt
REALWORLD_RELEVANT_OUTPUT=$DATA_DIR/Realworld/Results/realworld-relevant.csv
REALWORLD_SECURITY_OUTPUT=$DATA_DIR/Realworld/Results/realworld-relevant-hits-json.txt

# Collecting data

echo "$(date)"
echo 'Collecting Question IDs'
echo $QUESTION_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectQuestionsWithAndroidSubstringTags.groovy -p $POSTS_FILE -o $QUESTION_OUTPUT

echo "$(date)"
echo 'Collecting Answer IDs'
echo $ANSWER_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectAnswerIdFromQuestionIds.groovy  -p $POSTS_FILE -q $QUESTION_OUTPUT -o $ANSWER_OUTPUT

echo "$(date)"
echo 'Collecting Comment IDs'
echo $COMMENTS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectCommentIdsFromQnAIds.groovy -c $COMMENTS_FILE -a $ANSWER_OUTPUT -o $COMMENTS_OUTPUT

echo "$(date)"
echo 'Collecting Votes for Question, Answer, and Comment IDs'
echo $VOTES_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectVotesForGivenQAids.groovy -c $COMMENTS_OUTPUT -v $VOTES_FILE -o $VOTES_OUTPUT

echo "$(date)"
echo 'Collecting filtered Question IDs'
echo $FILTER_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/filterQids.groovy -p $POSTS_FILE -c $COMMENTS_OUTPUT -a $ANSWER_OUTPUT -v $VOTES_OUTPUT -f $COMMENTS_FILE -o $FILTER_OUTPUT

echo "$(date)"
echo 'Collecting SECURITY_TAGS_AND_SYNONYMS'
echo $SECURITY_TAGS_AND_SYNONYMS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectSecurityTagsAndSynonyms.groovy -t $SECURITY_TAGS -s $SECURITY_SYNONYMS -o $SECURITY_TAGS_AND_SYNONYMS_OUTPUT

echo "$(date)"
echo 'Collecting Security Question IDs'
echo $SECURITY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectAndroidSecurityQuestions.groovy -p $POSTS_FILE -s $SECURITY_TAGS_AND_SYNONYMS_OUTPUT -f $FILTER_OUTPUT -o $SECURITY_OUTPUT

echo "$(date)"
echo 'Collecting Filter-1 Questions and Answers'
echo $FILTER_1_OUTPUT_QUESTIONS
echo $FILTER_1_OUTPUT_ANSWERS
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/filter-1.groovy -p $POSTS_FILE -a $ANSWER_OUTPUT -f $FILTER_OUTPUT -q $FILTER_1_OUTPUT_QUESTIONS -z $FILTER_1_OUTPUT_ANSWERS

echo "$(date)"
echo 'Collecting body for Question IDs'
echo $BODY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectBodyForGivenIds.groovy -p $POSTS_FILE -q $FILTER_1_OUTPUT_QUESTIONS -z $FILTER_1_OUTPUT_ANSWERS -o $BODY_OUTPUT

echo "$(date)"
echo 'Collecting Creation and Activity Dates'
echo $CREATION_AND_ACTIVITY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectCreationAndActivityDate.groovy -p $POSTS_FILE -f $FILTER_OUTPUT -o $CREATION_AND_ACTIVITY_OUTPUT

echo "$(date)"
echo 'Collecting hits for GHERA APIs'
echo $GHERA_HITS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectHitsParallel.groovy -b $BODY_OUTPUT -a $GHERA_APIS -o $GHERA_HITS_OUTPUT
echo "$(date)"

echo "$(date)"
echo 'Collecting score for GHERA APIs'
echo $GHERA_RELEVANT_OUTPUT
echo $GHERA_HITS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseParallel.groovy -h $GHERA_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -o $GHERA_RELEVANT_OUTPUT

echo "$(date)"
echo 'Collecting score for GHERA APIs -- Security'
echo $GHERA_SECURITY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseSecurityParallel.groovy -h $GHERA_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -s $SECURITY_OUTPUT -o $GHERA_SECURITY_OUTPUT

echo 'Collecting data for DROIDBENCH'

echo "$(date)"
echo 'Collecting hits for DROIDBENCH APIs'
echo $DROIDBENCH_HITS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectHitsParallel.groovy -b $BODY_OUTPUT -a $DROID_APIS -o $DROIDBENCH_HITS_OUTPUT

echo "$(date)"
echo 'Collecting score for DROIDBENCH APIs'
echo $DROIDBENCH_RELEVANT_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseParallel.groovy -h $DROIDBENCH_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -o $DROIDBENCH_RELEVANT_OUTPUT

echo "$(date)"
echo 'Collecting score for DROIDBENCH APIs -- Security'
echo $DROIDBENCH_SECURITY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseSecurityParallel.groovy -h $DROIDBENCH_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -s $SECURITY_OUTPUT -o $DROIDBENCH_SECURITY_OUTPUT

echo 'Collecting data for ICCBENCH'

echo "$(date)"
echo 'Collecting hits for ICCBENCH APIs'
echo $ICCBENCH_HITS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectHitsParallel.groovy -b $BODY_OUTPUT -a $ICC_APIS -o $ICCBENCH_HITS_OUTPUT

echo "$(date)"
echo 'Collecting score for ICCBENCH APIs'
echo $ICCBENCH_RELEVANT_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseParallel.groovy -h $ICCBENCH_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -o $ICCBENCH_RELEVANT_OUTPUT

echo "$(date)"
echo 'Collecting score for ICCBENCH APIs -- Security'
echo $ICCBENCH_SECURITY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseSecurityParallel.groovy -h $ICCBENCH_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -s $SECURITY_OUTPUT -o $ICCBENCH_SECURITY_OUTPUT

echo 'Collecting data for UBCBENCH'

echo "$(date)"
echo 'Collecting hits for UBCBENCH APIs'
echo $UBCBENCH_HITS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectHitsParallel.groovy -b $BODY_OUTPUT -a $UBC_APIS -o $UBCBENCH_HITS_OUTPUT

echo "$(date)"
echo 'Collecting score for UBCBENCH APIs'
echo $UBCBENCH_RELEVANT_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseParallel.groovy -h $UBCBENCH_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -o $UBCBENCH_RELEVANT_OUTPUT

echo "$(date)"
echo 'Collecting score for UBCBENCH APIs -- Security'
echo $UBCBENCH_SECURITY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseSecurityParallel.groovy -h $UBCBENCH_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -s $SECURITY_OUTPUT -o $UBCBENCH_SECURITY_OUTPUT

echo "$(date)"
echo 'Collecting hits for Realworld APIs'
echo $REALWORLD_HITS_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectHitsParallel.groovy -b $BODY_OUTPUT -a $REAL_WORLD_APIS -o $REALWORLD_HITS_OUTPUT

echo "$(date)"
echo 'Collecting score for Realworld APIs'
echo $REALWORLD_RELEVANT_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseParallel.groovy -h $REALWORLD_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -o $REALWORLD_RELEVANT_OUTPUT

echo "$(date)"
echo 'Collecting score for Realworld APIs -- Security'
echo $REALWORLD_SECURITY_OUTPUT
C:/groovy-2.5.6/groovy-2.5.6/bin/groovy $SCRIPTS_DIR/collectYearWiseSecurityParallel.groovy -h $REALWORLD_HITS_OUTPUT -c $CREATION_AND_ACTIVITY_OUTPUT -s $SECURITY_OUTPUT -o $REALWORLD_SECURITY_OUTPUT

echo "$(date)"
echo 'Done!!!'
